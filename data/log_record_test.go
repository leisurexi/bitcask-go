package data

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEncodeLogRecord(t *testing.T) {
	// 正常去编码一条数据
	rec1 := &LogRecord{
		Key:   []byte("name"),
		Value: []byte("leisurexi"),
		Type:  LogRecordNormal,
	}

	res1, n1 := EncodeLogRecord(rec1)
	assert.NotNil(t, res1)
	assert.Greater(t, n1, int64(5))

	// value 为空的情况
	rec2 := &LogRecord{
		Key:  []byte("name2"),
		Type: LogRecordNormal,
	}

	res2, n2 := EncodeLogRecord(rec2)
	assert.NotNil(t, res2)
	assert.Greater(t, n2, int64(5))

	// 对 Deleted 情况的测试
	rec3 := &LogRecord{
		Key:  []byte("name2"),
		Type: LogRecordDeleted,
	}
	res3, n3 := EncodeLogRecord(rec3)
	assert.NotNil(t, res3)
	assert.Greater(t, n3, int64(5))
}

func TestDecodeLogRecordHeader(t *testing.T) {
	headerBuf1 := []byte{198, 19, 214, 147, 0, 8, 18, 110, 97, 109, 101, 108, 101, 105, 115, 117, 114, 101, 120, 105}
	header, n := decodeLogRecordHeader(headerBuf1)
	assert.NotNil(t, header)
	assert.Equal(t, int64(7), n)
	assert.Equal(t, uint32(2480280518), header.crc)
	assert.Equal(t, LogRecordNormal, header.recordType)
	assert.Equal(t, uint32(4), header.keySize)
	assert.Equal(t, uint32(9), header.valueSize)
}
