package bitcask_go

type Options struct {
	DirPath      string    // 数据文件目录
	DataFileSize int64     // 数据库文件大小
	IndexType    IndexType // 内存索引类型
	SyncWrites   bool      // 每次写数据是否持久化
}

type IndexType = int8

const (
	Btree IndexType = iota + 1
	ART             // 自适应基数树索引
)
