package fio

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewFileIOManager(t *testing.T) {
	fileIOManager, err := NewFileIOManager("./a.data")
	assert.Nil(t, err)
	assert.NotNil(t, fileIOManager)
}

func TestFileIO_Write(t *testing.T) {
	fileIOManager, err := NewFileIOManager("./a.data")
	assert.Nil(t, err)
	assert.NotNil(t, fileIOManager)

	n, err := fileIOManager.Write([]byte("1"))
	assert.Equal(t, 1, n)
	assert.Nil(t, err)
	n, err = fileIOManager.Write([]byte("23"))
	assert.Equal(t, 2, n)
	assert.Nil(t, err)
	n, err = fileIOManager.Write([]byte("456"))
	assert.Equal(t, 3, n)
	assert.Nil(t, err)
}

func TestFileIO_Read(t *testing.T) {
	fileIOManager, err := NewFileIOManager("./a.data")
	assert.Nil(t, err)
	assert.NotNil(t, fileIOManager)

	b1 := make([]byte, 3)
	n, err := fileIOManager.Read(b1, 0)
	assert.Equal(t, 3, n)
	assert.Equal(t, "123", string(b1))
	assert.Nil(t, err)

	b2 := make([]byte, 3)
	n, err = fileIOManager.Read(b2, 3)
	t.Log(string(b2))
	assert.Equal(t, 3, n)
	assert.Equal(t, "456", string(b2))
	assert.Nil(t, err)
}

func TestFileIO_Sync(t *testing.T) {
	fileIOManager, err := NewFileIOManager("./a.data")
	assert.Nil(t, err)
	assert.NotNil(t, fileIOManager)

	n, err := fileIOManager.Write([]byte("1"))
	assert.Equal(t, 1, n)
	assert.Nil(t, err)

	err = fileIOManager.Sync()
	assert.Nil(t, err)
}

func TestFileIO_Close(t *testing.T) {
	fileIOManager, err := NewFileIOManager("./a.data")
	assert.Nil(t, err)
	assert.NotNil(t, fileIOManager)

	n, err := fileIOManager.Write([]byte("1"))
	assert.Equal(t, 1, n)
	assert.Nil(t, err)

	err = fileIOManager.Close()
	assert.Nil(t, err)
}
