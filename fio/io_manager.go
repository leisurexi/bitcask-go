package fio

// IOManager 抽象 IO 管理接口，可以接入不同的 IO 类型
type IOManager interface {
	// Read 从文件的指定位置读取数据
	Read([]byte, int64) (int, error)
	// Write 写入数据到文件中
	Write([]byte) (int, error)
	// Sync 持久化数据
	Sync() error
	// Close 关闭 IO
	Close() error
	// Size 获取文件大小
	Size() (int64, error)
}

// NewIOManager 初始化 IOManager，目前只支持标准 FileIO
func NewIOManager(fileName string) (IOManager, error) {
	return NewFileIOManager(fileName)
}
